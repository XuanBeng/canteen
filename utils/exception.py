from django.http import Http404
from rest_framework.exceptions import PermissionDenied
from rest_framework.views import set_rollback
from rest_framework import exceptions, status

from utils.response import ApiResponse
def exception_handler(exc, context):
    response = drf_exception_handler(exc, context)
    if response is None:
        errMessage =  context['view'], context['request'].method, exc
        print(errMessage)
        return ApiResponse(data_status=1, data_msg=errMessage)
    return response


def drf_exception_handler(exc, context):
    """
    Returns the response that should be used for any given exception.

    By default we handle the REST framework `APIException`, and also
    Django's built-in `Http404` and `PermissionDenied` exceptions.

    Any unhandled exceptions may return `None`, which will cause a 500 error
    to be raised.
    """
    if isinstance(exc, Http404):
        exc = exceptions.NotFound()
    elif isinstance(exc, PermissionDenied):
        exc = exceptions.PermissionDenied()

    if isinstance(exc, exceptions.APIException):
        headers = {}
        if getattr(exc, 'auth_header', None):
            headers['WWW-Authenticate'] = exc.auth_header
        if getattr(exc, 'wait', None):
            headers['Retry-After'] = '%d' % exc.wait

        if isinstance(exc.detail, (list, dict)):
            data = exc.detail
        else:
            data = {'detail': exc.detail}

        set_rollback()
        return ApiResponse(data_status=1, data_msg=data , http_status=exc.status_code, headers=headers)

    return None
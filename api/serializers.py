import re

from rest_framework import serializers, status
from rest_framework_jwt.settings import api_settings
from rest_framework.exceptions import ValidationError
from utils.ApiExceptionError import ApiExceptionError, ApiValidationError

from . import models

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER

class UserLoginSerializer(serializers.ModelSerializer):

    # 自定义反序列化字段，查询无关
    account = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    class Meta:
        model = models.User
        fields = ['account', 'password']
        extra_kwargs = {

        }

    def validate(self, attrs):
        account = attrs.get('account')
        password = attrs.get('password')

        if re.match(r'.+@.+', account):
            user_query = models.User.objects.filter(email= account)
        elif re.match(r'1[3-9][0-9]{9}', account):
            user_query = models.User.objects.filter(mobile=account)
        else:
            user_query = models.User.objects.filter(username=account)
        user = user_query.first()
        if user:
            if user.check_password(password):
                # 签发token
                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                self.token = token
                print(token)
                return user
            else:
                # 后期自定义封装错误信息
                # raise ApiExceptionError('password is wrong')
                raise ApiValidationError(detail={'detail': 'password is wrong'}, code= status.HTTP_200_OK)
                # raise serializers.ValidationError(detail={'detail': 'password is wrong'}, code= status.HTTP_200_OK)
        else:
            # raise ApiExceptionError('account is not exist or wrong')
            raise ApiValidationError(detail={'detail': 'account is not exist'}, code= status.HTTP_200_OK)
            # raise serializers.ValidationError(detail={'detail': 'account is not exist'}, code= status.HTTP_200_OK)


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = ('id', 'username', 'is_active', 'date_joined', 'mobile','is_superuser')

    # def create(self, validated_data):
    #     username = validated_data.get('username')
    #     password = validated_data.get('password')
    #     mobile = validated_data.get('mobile')
    #     return models.User.objects.create_user(username=username, password=password, mobile=mobile)

    # def validate_mobile(self, value):
    #     # mobile = attrs.get('mobile')
    #     if models.User.objects.filter(mobile=value):
    #         raise ValidationError('该手机号已被注册')
        # fields = ['username', 'password', ]
        # extra_kwargs = {
        #     'password': {
        #         'write_only': True
        #     },
        #     'food_obj': {
        #         'read_only': True
        #     }
        # }

class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Food
        # 参与序列化的字段
        fields = '__all__'

class SeatSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Seat
        # 参与序列化的字段
        fields = '__all__'

class TzDetailSerializer(serializers.ModelSerializer):
    # food = FoodSerializer(read_only=True)
    class Meta:
        model = models.TzDetail
        # 参与序列化的字段
        # fields = '__all__'
        fields = ('id', 'food', 'amount', 'food_obj', 'tz')
        extra_kwargs = {
            'food': {
                'write_only': True
            },
            'food_obj': {
                'read_only': True
            }
        }

class TzSerializer(serializers.ModelSerializer):
    tz_details = TzDetailSerializer(many=True, read_only=True)
    class Meta:
        model = models.Tz
        # 参与序列化的字段
        # fields = '__all__'
        fields = ('id', 'seat', 'status', 'total_price', 'tz_details')
        # extra_kwargs = {
        #     'tz_details': {
        #         'read_only': True
        #     }
        # }



# Create your views here.
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from utils.auth import JwtAuthentication
from . import serializers, models
from utils.response import ApiResponse
from utils.modelViewSet import ApiModelViewSet
from utils.ApiExceptionError import ApiExceptionError

class UserDetail(ApiModelViewSet):

    # authentication_classes = [JwtAuthentication]
    # permission_classes = [IsAuthenticated]
    # def get(self, request, *args, **kwargs):
    #     return ApiResponse(data_res=request.user.username)
    #

    queryset = models.User.objects.all()
    serializer_class = serializers.UserDetailSerializer

    def post(self,  request, *args, **kwargs):
        try:
            username = request.data.get('username')
            password = request.data.get('password')
            mobile = request.data.get('mobile')
            if username:
                if models.User.objects.filter(username= username):
                    raise ApiExceptionError('用户名已存在')
            else:
                raise ApiExceptionError('用户名为空')

            if mobile:
                if models.User.objects.filter(mobile= mobile):
                    raise ApiExceptionError('手机号已存在')
            else:
                raise ApiExceptionError('手机号为空')

            if password == None:
                raise ApiExceptionError('密码为空')
            models.User.objects.create_user(username=username, password=password, mobile=mobile)
            return ApiResponse()
        except ApiExceptionError as e:
            print(e.msg)
            return ApiResponse(data_status=1, data_msg=e.msg)


class UserLoginView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        ser = serializers.UserLoginSerializer(data= request.data)
        if ser.is_valid(raise_exception=False):
            return ApiResponse(token=ser.token)
        else:
            return ApiResponse(data_status=1, data_msg=ser.errors)

class FoodApiView(ApiModelViewSet):
    queryset = models.Food.objects.filter(is_delete=False).all()
    serializer_class = serializers.FoodSerializer


class SeatApiView(ApiModelViewSet):
    queryset = models.Seat.objects.all()
    serializer_class = serializers.SeatSerializer

class TzApiView(ApiModelViewSet):
    queryset = models.Tz.objects.filter(is_delete=False, status='0').all()
    serializer_class = serializers.TzSerializer

class TzDetailApiView(ApiModelViewSet):
    queryset = models.TzDetail.objects.all()
    serializer_class = serializers.TzDetailSerializer

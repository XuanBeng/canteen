from django.conf.urls import url
from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from . import views

urlpatterns = [    url(r'^login/$', views.UserLoginView.as_view()),
    url(r'^user_detail/$', views.UserDetail.as_view()),
    url(r'^food/$', views.FoodApiView.as_view()),
    url(r'^food/(?P<pk>.*)/$', views.FoodApiView.as_view()),
    url(r'^seat/', views.SeatApiView.as_view()),
    url(r'^tz/$', views.TzApiView.as_view()),
    url(r'^tz/(?P<pk>.*)/$', views.TzApiView.as_view()),
    url(r'^tz_detail/', views.TzDetailApiView.as_view())
]

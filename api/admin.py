from django.contrib import admin

from . import models
# Register your models here.
admin.site.register(models.User)
admin.site.register(models.Food)
admin.site.register(models.Seat)
admin.site.register(models.Tz)
admin.site.register(models.TzDetail)
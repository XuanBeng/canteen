from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

# 权限六表：用户表、角色表、权限表;
#          用户与角色关系表、用户与权限关系表、角色与权限关系表;

# 用户表：访问角色表字段名groups,访问权限字段名user_permissions;
# 角色表：用户user_set, 权限permissions;
# 权限表：用户user_set, 角色group_set;

class BaseModel(models.Model):
    # id = models.UUIDField(primary_key=True, auto_created=True, default=uuid.uuid4, editable=False)
    is_delete = models.BooleanField(default=False)
    create_date = models.DateTimeField(auto_now_add=True)
    remark = models.CharField(max_length=255, null=True,blank=True)
    class Meta:
        abstract = True


class User(AbstractUser):
    mobile = models.CharField(max_length=11, unique=True)

    class Meta:
        db_table = "api_user"
        verbose_name = "用户表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username


class Food(BaseModel):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=6, decimal_places=2, null=True,blank=True)
    # img = models.ImageField(upload_to='img', default='img/default.jpg')
    class Meta:
        db_table = "c_food"
        verbose_name = "菜品表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

class Seat(BaseModel):
    number = models.CharField(max_length=4)
    people_no = models.IntegerField()

    class Meta:
        db_table = "c_seat"
        verbose_name = "座位表"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.number

class Tz(BaseModel):
    seat = models.ForeignKey(to='Seat', on_delete=models.DO_NOTHING, db_constraint=False, related_name="tzs")
    status = models.CharField(max_length=1)
    total_price = models.DecimalField(max_digits=6, decimal_places=2, null=True,blank=True)
    leaveTime = models.DateTimeField(null=True,blank=True)

    # @property
    # def tz_details(self):
    #     tz_details = self.tz_details().values()
    #     return tz_details
    class Meta:
        db_table = "food_tz"
        verbose_name = "台账"
        verbose_name_plural = verbose_name



class TzDetail(BaseModel):
    tz = models.ForeignKey(to='Tz', on_delete= models.DO_NOTHING, db_constraint=False, related_name="tz_details")
    food = models.ForeignKey(to='Food', on_delete= models.DO_NOTHING, db_constraint=False)
    amount = models.IntegerField(null=True,blank=True)

    @property
    def food_obj(self):
        food_obj = {}
        food_obj['name'] = self.food.name
        food_obj['type'] = self.food.type
        food_obj['price'] = self.food.price
        return food_obj
    class Meta:
        db_table = "tz_detail"
        verbose_name = "台账细则"
        verbose_name_plural = verbose_name
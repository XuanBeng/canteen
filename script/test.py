import os, django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'drf.settings')
django.setup()


from api import models
from django.contrib.auth.models import Group, Permission

user = models.User.objects.first()
print(user.username)
print(user.groups.all())